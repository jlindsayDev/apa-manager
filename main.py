"""
  Josh Lindsay
"""
from argparse import ArgumentParser
from requests import Session
import apa_ops as APA


def getArgs():
    parser = ArgumentParser()
    parser.add_argument('-u', '--username', action='store', required=True)
    parser.add_argument('-p', '--password', action='store', required=True)
    return parser.parse_args()

def main():
    args = getArgs()
    s = Session()
    r = APA.login(s, args.username, args.password)
    r = APA.get_statistics(s, r)
    r = APA.logout(s, r)

main()
